package com.clabz.medium.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project Name : medium-api-news-service
 * Author : ChinthakaDi
 * Date : 10/27/2018 - 1:07 PM
 */
@RestController
@RequestMapping("/api/v1")
public class NewsController {

    @GetMapping("/find/latest/news")
    public String getNewsFromAPI(){
        return "NEWS From Core News Service";
    }

}
