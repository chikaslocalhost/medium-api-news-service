package com.clabz.medium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.xmlpull.v1.XmlPullParserException;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
public class MediumApiNewsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediumApiNewsServiceApplication.class, args);
    }

    @Bean
    public Docket api() throws IOException, XmlPullParserException {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.clabz.medium.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfo("News Service Api Documentation", "Documentation automatically generated", "1.0.0", null, null, null, null));
    }


}
